/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"
#include <string>

//==============================================================================
InstantLofiAudioProcessor::InstantLofiAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                     #endif
                       ), tree(*this, nullptr, "PARAMETER", createParameterLayout())
#endif
{
    
}

InstantLofiAudioProcessor::~InstantLofiAudioProcessor()
{
}

AudioProcessorValueTreeState::ParameterLayout InstantLofiAudioProcessor::createParameterLayout()
{
    NormalisableRange<float> cutoffRange(20.0f, 20000.0f);
    NormalisableRange<float> resRange(1.0f, 5.0f);
    
    std::vector <std::unique_ptr<RangedAudioParameter>> params;
    
    auto cutoffParam = std::make_unique<AudioParameterFloat>("cutoff", "Cutoff", cutoffRange, 600.0f);
    auto resParam = std::make_unique<AudioParameterFloat>("resonance", "Resonance", resRange, 1.0f);
    
    auto filterMenuParam = std::make_unique<AudioParameterChoice>("filterMenu", "Filter Menu", StringArray ("LowPass", "BandPass", "HighPass"), 0);
    
    params.push_back(std::move(cutoffParam));
    params.push_back(std::move(resParam));
    params.push_back(std::move(filterMenuParam));
    
    return { params.begin(), params.end() };
}

//==============================================================================
const String InstantLofiAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

double InstantLofiAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int InstantLofiAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int InstantLofiAudioProcessor::getCurrentProgram()
{
    return 0;
}

void InstantLofiAudioProcessor::setCurrentProgram (int index)
{
}

const String InstantLofiAudioProcessor::getProgramName (int index)
{
    return {};
}

void InstantLofiAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void InstantLofiAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // PRE-PROCESSING
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
    lastSampleRate = sampleRate;
    
    dsp::ProcessSpec spec;
    spec.sampleRate = lastSampleRate;
    spec.maximumBlockSize = samplesPerBlock;
    spec.numChannels = getMainBusNumOutputChannels();
    
    stateVariableFilter.reset();
    updateFilter();
    stateVariableFilter.prepare(spec);
}

void InstantLofiAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

void InstantLofiAudioProcessor::updateFilter()
{
    auto menuChoice = *tree.getRawParameterValue("filterMenu");
    auto cutoff = *tree.getRawParameterValue("cutoff");
    auto res = *tree.getRawParameterValue("resonance");
    
    if (menuChoice == 0)
    {
        stateVariableFilter.state->type = dsp::StateVariableFilter::Parameters<float>::Type::lowPass;

        stateVariableFilter.state->setCutOffFrequency (lastSampleRate, cutoff, res);
    }

    if (menuChoice == 1)
    {
        stateVariableFilter.state->type = dsp::StateVariableFilter::Parameters<float>::Type::bandPass;

        stateVariableFilter.state->setCutOffFrequency (lastSampleRate, cutoff, res);
    }

    if (menuChoice == 2)
    {
        stateVariableFilter.state->type = dsp::StateVariableFilter::Parameters<float>::Type::highPass;

        stateVariableFilter.state->setCutOffFrequency (lastSampleRate, cutoff, res);
    }

}

#ifndef JucePlugin_PreferredChannelConfigurations
bool InstantLofiAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void InstantLofiAudioProcessor::processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages)
{
    ScopedNoDenormals noDenormals;
    const int totalNumInputChannels  = getTotalNumInputChannels();
    const int totalNumOutputChannels = getTotalNumOutputChannels();
    
    for (int i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

    dsp::AudioBlock<float> block (buffer);
    
    updateFilter();
    stateVariableFilter.process(dsp::ProcessContextReplacing<float> (block));
}

//==============================================================================
bool InstantLofiAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* InstantLofiAudioProcessor::createEditor()
{
    return new InstantLofiAudioProcessorEditor (*this);
}

//==============================================================================
void InstantLofiAudioProcessor::getStateInformation (MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

void InstantLofiAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new InstantLofiAudioProcessor();
}
