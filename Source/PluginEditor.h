/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"

//==============================================================================
/**
*/
class InstantLofiAudioProcessorEditor  : public AudioProcessorEditor
{
public:
    InstantLofiAudioProcessorEditor (InstantLofiAudioProcessor&);
    ~InstantLofiAudioProcessorEditor();

    //==============================================================================
    void paint (Graphics&) override;
    void resized() override;

    
private:
    InstantLofiAudioProcessor& processor;
    
    Slider filterCutoffDial;
    Slider filterResDial;
    
    ComboBox filterMenu;
    
    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> filterCutoffValue;
    std::unique_ptr <AudioProcessorValueTreeState::SliderAttachment> filterResValue;
    
    std::unique_ptr <AudioProcessorValueTreeState::ComboBoxAttachment> filterMenuChoice;
    
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (InstantLofiAudioProcessorEditor)
};
