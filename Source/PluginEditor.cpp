/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"
#include <string>

//==============================================================================
InstantLofiAudioProcessorEditor::InstantLofiAudioProcessorEditor (InstantLofiAudioProcessor& p)
    : AudioProcessorEditor (&p), processor (p)
{
    // Filter Menu
    filterMenu.setJustificationType(Justification::centred);
    filterMenu.addItem("Low Pass", 1);
    filterMenu.addItem("Band Pass", 2);
    filterMenu.addItem("High Pass", 3);
    addAndMakeVisible(&filterMenu);
    
    // Filter Cut Off dial
    filterCutoffDial.setSliderStyle(Slider::SliderStyle::RotaryVerticalDrag);
    filterCutoffDial.setTextBoxStyle(Slider::NoTextBox, false, 0, 0);
    filterCutoffDial.setPopupDisplayEnabled(true, true, this);
    addAndMakeVisible(&filterCutoffDial);
    
    // Filter Resonance Dial
    filterResDial.setSliderStyle(Slider::SliderStyle::RotaryVerticalDrag);
    filterResDial.setTextBoxStyle(Slider::NoTextBox, false, 0, 0);
    filterResDial.setPopupDisplayEnabled(true, true, this);
    addAndMakeVisible(&filterResDial);
    
    filterCutoffValue = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(processor.tree, "cutoff", filterCutoffDial);
    
    filterResValue = std::make_unique<AudioProcessorValueTreeState::SliderAttachment>(processor.tree, "resonance", filterResDial);
    
    filterMenuChoice = std::make_unique<AudioProcessorValueTreeState::ComboBoxAttachment>(processor.tree, "filterMenu", filterMenu);
    
    filterCutoffDial.setSkewFactor(1000.0f);
    
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (900, 600);
}

InstantLofiAudioProcessorEditor::~InstantLofiAudioProcessorEditor()
{
}

//==============================================================================
void InstantLofiAudioProcessorEditor::paint (Graphics& g)
{
    // background ui stuff
    Rectangle<int> titleArea (0, 10, getWidth(), 20);
    
    g.fillAll(Colours::black);
    g.setColour(Colours::white);
    g.drawText("Filter", titleArea, Justification::centredTop);
    g.drawText("Cutoff", 46, 70, 50, 25, Justification::centredLeft);
    g.drawText("Resonance", 107, 70, 70, 25, Justification::centredLeft);
    
    Rectangle <float> area (25, 25, 150, 150);

    g.setColour (Colours::yellow);
    g.drawRoundedRectangle(area, 20.0f, 2.0f);
}

void InstantLofiAudioProcessorEditor::resized()
{
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..
    Rectangle<int> area = getLocalBounds().reduced(40);
    
    filterMenu.setBounds(area.removeFromTop(20));
    filterCutoffDial.setBounds(30, 90, 70, 70);
    filterResDial.setBounds(100, 90, 70, 70);
}
